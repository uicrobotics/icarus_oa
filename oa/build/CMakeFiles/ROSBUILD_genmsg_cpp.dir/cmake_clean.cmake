FILE(REMOVE_RECURSE
  "../msg_gen"
  "../srv_gen"
  "../src/oa/msg"
  "../src/oa/srv"
  "../msg_gen"
  "../srv_gen"
  "CMakeFiles/ROSBUILD_genmsg_cpp"
  "../msg_gen/cpp/include/oa/Attitude.h"
  "../msg_gen/cpp/include/oa/Mavlink_RAW_IMU.h"
  "../msg_gen/cpp/include/oa/GPS.h"
  "../msg_gen/cpp/include/oa/DataToFC.h"
  "../msg_gen/cpp/include/oa/Control.h"
  "../msg_gen/cpp/include/oa/DataToFCGPS.h"
  "../msg_gen/cpp/include/oa/VFR_HUD.h"
  "../msg_gen/cpp/include/oa/DataFromFC.h"
  "../msg_gen/cpp/include/oa/RC.h"
  "../msg_gen/cpp/include/oa/Position.h"
  "../msg_gen/cpp/include/oa/State.h"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/ROSBUILD_genmsg_cpp.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
