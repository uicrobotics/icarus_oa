FILE(REMOVE_RECURSE
  "../msg_gen"
  "../srv_gen"
  "../src/oa/msg"
  "../src/oa/srv"
  "../msg_gen"
  "../srv_gen"
  "CMakeFiles/ROSBUILD_genmsg_py"
  "../src/oa/msg/__init__.py"
  "../src/oa/msg/_Attitude.py"
  "../src/oa/msg/_Mavlink_RAW_IMU.py"
  "../src/oa/msg/_GPS.py"
  "../src/oa/msg/_DataToFC.py"
  "../src/oa/msg/_Control.py"
  "../src/oa/msg/_DataToFCGPS.py"
  "../src/oa/msg/_VFR_HUD.py"
  "../src/oa/msg/_DataFromFC.py"
  "../src/oa/msg/_RC.py"
  "../src/oa/msg/_Position.py"
  "../src/oa/msg/_State.py"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/ROSBUILD_genmsg_py.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
