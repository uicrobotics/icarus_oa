(cl:in-package oa-msg)
(cl:export '(TIME-VAL
          TIME
          LATITUDE-VAL
          LATITUDE
          LONGITUDE-VAL
          LONGITUDE
          ALTITUDE-VAL
          ALTITUDE
))