(cl:in-package oa-msg)
(cl:export '(AIRSPEED-VAL
          AIRSPEED
          GROUNDSPEED-VAL
          GROUNDSPEED
          HEADING-VAL
          HEADING
          THROTTLE-VAL
          THROTTLE
          ALT-VAL
          ALT
          CLIMB-VAL
          CLIMB
))