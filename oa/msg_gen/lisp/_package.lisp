(cl:defpackage oa-msg
  (:use )
  (:export
   "<ATTITUDE>"
   "ATTITUDE"
   "<MAVLINK_RAW_IMU>"
   "MAVLINK_RAW_IMU"
   "<GPS>"
   "GPS"
   "<DATATOFC>"
   "DATATOFC"
   "<CONTROL>"
   "CONTROL"
   "<DATATOFCGPS>"
   "DATATOFCGPS"
   "<VFR_HUD>"
   "VFR_HUD"
   "<DATAFROMFC>"
   "DATAFROMFC"
   "<RC>"
   "RC"
   "<POSITION>"
   "POSITION"
   "<STATE>"
   "STATE"
  ))

