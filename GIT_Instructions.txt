Update local machine to repo latest code:
1. git add . (or git add <filename>)
2. git pull

Update repo to local machine's latest code:
1. git add .
2. git commit -m "Commit message"
3. git push origin master
